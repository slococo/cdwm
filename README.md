# cdwm

Heavily-patched build of `dwm`.

## Table of contents
  - [Requirements <a name="requirements"></a>](#requirements-)
  - [Installation <a name="installation"></a>](#installation-)
  - [Usage <a name="usage"></a>](#usage-)
  - [Patches <a name="patches"></a>](#patches-)
  - [Contributing <a name="contributing"></a>](#contributing-)
  - [License <a name="license"></a>](#license-)

## Requirements <a name="requirements"></a>

In order to build `dwm` you need the `Xlib` header files and [libxft-bgra][1].

## Installation <a name="installation"></a>

You need to run:

```bash
make clean install  
```

## Usage <a name="usage"></a>

To run `cdwm`:

```bash
exec dwm
```

## Patches <a name="patches"></a>

- Clickable statusbar.
- Scratchpad.
- True fullscreen.
- Sticky windows.
- Stacker.
- Attach direction.
- Shiftview.
- Always center.
- Gaps.
- Sticky.

## Contributing <a name="contributing"></a>
PRs are welcome.

## License <a name="license"></a>
[MIT](https://raw.githubusercontent.com/santilococo/cdwm/master/LICENSE)

[1]: https://aur.archlinux.org/packages/libxft-bgra

