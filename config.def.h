/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
//static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
//static const unsigned int gappih    = 20;       /* horiz inner gap between windows */
static const unsigned int gappih    = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
//static const unsigned int gappov    = 30;       /* vert outer gap between windows and screen edge */
//static const unsigned int gappov    = 15;       /* vert outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
//static const char *fonts[]          = { "monospace:size=10"};
static const char *fonts[]          = { "monospace:size=12", "Noto Color Emoji:pixelsize=12:antialias=true:autohint=true" };
static const char dmenufont[]       = "monospace:size=12";
/*
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char col1[]            = "#ffffff";
static const char col2[]            = "#ffffff";
static const char col3[]            = "#ffffff";
static const char col4[]            = "#ffffff";
static const char col5[]            = "#ffffff";
static const char col6[]            = "#ffffff";
*/
//#include "themes/dracula.h"
//#include "themes/ocean.h"
#include "themes/onedark.h"

enum { SchemeNorm, SchemeCol1, SchemeCol2, SchemeCol3, SchemeCol4,
       SchemeCol5, SchemeCol6, SchemeSel }; /* color schemes */

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm]  = { col_gray3, col_gray1, col_gray2 },
	/*
	[SchemeCol1]  = { col1,      col_gray1, col_gray2 },
	[SchemeCol2]  = { col2,      col_gray1, col_gray2 },
	[SchemeCol3]  = { col3,      col_gray1, col_gray2 },
	[SchemeCol4]  = { col4,      col_gray1, col_gray2 },
	[SchemeCol5]  = { col5,      col_gray1, col_gray2 },
	[SchemeCol6]  = { col6,      col_gray1, col_gray2 },
	*/
	[SchemeSel]   = { col_gray4, col_cyan,  col_cyan  },
};

typedef struct {
	const char *name;
	const void *cmd;
} Sp;
#ifdef LAPTOP
const char *spcmd1[] = {"st", "-n", "spterm", "-g", "80x24", NULL };
#else
const char *spcmd1[] = {"st", "-n", "spterm", "-g", "120x34", NULL };
#endif
const char *spcmd2[] = {"st", "-n", "spranger", "-g", "144x41", "-e", "ranger", NULL };
//const char *spcmd2[] = {"st", "-n", "sppcmanfm", "-g", "144x41", "-e", "/home/slococo/launchPcmanfm", NULL };
//const char *spcmd2[] = {"pcmanfm", NULL };
const char *spcmd3[] = {"bitwarden-desktop", NULL };
static Sp scratchpads[] = {
	/* name          cmd  */
	{"spterm",      spcmd1},
	{"spranger",    spcmd2},
	//{"sppcmanfm",    spcmd2},
	{"bitwarden",   spcmd3},
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
        /* class      instance    title       tags mask     isfloating   monitor */
        { "Gimp",     NULL,       NULL,       0,            1,          -1 },
        { "Firefox",  NULL,       NULL,       1 << 8,       0,          -1 },
	{ NULL,	    "spterm",	NULL,	    SPTAG(0),	    1,          -1 },
	{ NULL,   "spranger",	NULL,	    SPTAG(1),	    1,		-1 },
	{ NULL,	"bitwarden",	NULL,	    SPTAG(2),       1,          -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int attachdirection = 2;    /* 0 default, 1 above, 2 aside, 3 below, 4 bottom, 5 top */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
#include "shiftview.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	{ "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ "H[]",      deck },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "HHH",      grid },
	{ "###",      nrowgrid },
	{ "---",      horizgrid },
	{ ":::",      gaplessgrid },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};

/* key definitions */
#ifdef LAPTOP
#define MODKEY Mod4Mask
#else
#define MODKEY Mod1Mask
#endif
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
	{ MOD, XK_j,     ACTION##stack, {.i = INC(+1) } }, \
	{ MOD, XK_k,     ACTION##stack, {.i = INC(-1) } }, \
	{ MOD, XK_a,     ACTION##stack, {.i = 0 } }, \
	/* { MOD, XK_a,     ACTION##stack, {.i = 1 } }, \
	{ MOD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \
	{ MOD, XK_z,     ACTION##stack, {.i = 2 } }, \
	{ MOD, XK_x,     ACTION##stack, {.i = -1 } }, */

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };

/* https://cgit.freedesktop.org/xorg/proto/x11proto/tree/XF86keysym.h */
static Key keys[] = {
	/* modifier                     key        function        argument */
	//{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_o,      spawn,          SHCMD("rbw-menu") },
	{ MODKEY,                       XK_p,      spawn,          SHCMD("dmenu_run_history") },
	{ MODKEY|ShiftMask,	        XK_p,      spawn,          SHCMD("dmenu_terminal") },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	STACKKEYS(MODKEY,                          focus)
	STACKKEYS(MODKEY|ShiftMask,                push)
	{ MODKEY|ShiftMask,             XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_h,      setcfact,       {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_l,      setcfact,       {.f = -0.25} },
	{ MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,			XK_g,		shiftview,	{ .i = -1 } },
	{ MODKEY|ShiftMask,		XK_g,		shifttag,	{ .i = -1 } },
	{ MODKEY,			XK_semicolon,	shiftview,	{ .i = +1 } },
	{ MODKEY|ShiftMask,		XK_semicolon,	shifttag,	{ .i = +1 } },
#ifdef LAPTOP
	{ MODKEY|Mod1Mask,              XK_u,      incrgaps,       {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_i,      incrigaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_o,      incrogaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_6,      incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_7,      incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_8,      incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_9,      incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_0,      togglegaps,     {0} },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
#else
	{ MODKEY|Mod4Mask,              XK_u,      incrgaps,       {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_i,      incrigaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_o,      incrogaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_6,      incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_7,      incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_8,      incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_9,      incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_0,      togglegaps,     {0} },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
#endif
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_f,      fullscreen,     {0} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_s,      togglesticky,   {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,            			XK_x,  	   togglescratch,  {.ui = 0 } },
	{ MODKEY,            			XK_y,	   togglescratch,  {.ui = 1 } },
	{ MODKEY,            			XK_u,	   togglescratch,  {.ui = 2 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,	    quit,        {0} },
	{ MODKEY,			XK_q,      spawn,          SHCMD("kill -15 $(ps -u $USER -o pid,%mem,%cpu,command | sort -b -k2 -r | sed -n '1!p' | cut -b 1-75 | dmenu -l 15)") },
	{ 0,                       XF86XK_AudioLowerVolume, spawn,  SHCMD("pulsemixer --change-volume -1; sigdwmblocks 2") },
	{ 0,                       XF86XK_AudioRaiseVolume, spawn,  SHCMD("pulsemixer --change-volume +1; sigdwmblocks 2") },
	{ 0,                       XF86XK_AudioMute,	    spawn,  SHCMD("pulsemixer --toggle-mute; sigdwmblocks 2") },
	{ 0,                       XF86XK_AudioPlay,	    spawn,  SHCMD("$HOME/.local/bin/player play-pause") },
	{ 0,                       XF86XK_AudioNext,	    spawn,  SHCMD("$HOME/.local/bin/player next") },
	{ 0,                       XF86XK_AudioPrev,	    spawn,  SHCMD("$HOME/.local/bin/player previous") },
	{ 0,                       XF86XK_AudioStop,	    spawn,  SHCMD("$HOME/.local/bin/player --all-players stop") },
#ifdef LAPTOP
	{ 0,                       XF86XK_MonBrightnessDown,	    spawn,  SHCMD("xbacklight -dec 5") },
	{ 0,                       XF86XK_MonBrightnessUp,	    spawn,  SHCMD("xbacklight -inc 5") },
	{ 0,                       XF86XK_KbdBrightnessDown,	    spawn,  SHCMD("brightnessctl --device='smc::kbd_backlight' set 20%-") },
	{ 0,                       XF86XK_KbdBrightnessUp,	    spawn,  SHCMD("brightnessctl --device='smc::kbd_backlight' set +20%") },
#else
	{ 0,                       XF86XK_Mail,		    spawn,  SHCMD("$HOME/.local/bin/turnoffscreen") },
	{ 0,                       XF86XK_HomePage,	    spawn,  SHCMD("pcmanfm") },
	{ 0,                       XF86XK_Tools,	    spawn,  SHCMD("pkill imwheel && imwheel -b 45") },
	{ 0,                       XF86XK_AudioStop,	    spawn,  SHCMD("dunstctl set-paused toggle; sigdwmblocks 6") },
	{ 0,                       XK_Print,	    spawn,  SHCMD("$HOME/.local/bin/screenshot") },
#endif
	{ MODKEY|ShiftMask,             XK_m, spawn,          SHCMD("noisetorch; $TERMINAL -e \"alsamixer\"") },
	{ MODKEY,                       XK_v,	    spawn,       SHCMD("clipmenu") },
	//{ MODKEY|ControlMask,                       XK_c,	spawn,       SHCMD("$HOME/.local/bin/copyNoNewline") },
	{ MODKEY|ControlMask,                       XK_c,	spawn,       SHCMD("$HOME/.local/bin/copy") },
	{ MODKEY|ShiftMask|ControlMask,             XK_c,	spawn,       SHCMD("$HOME/.local/bin/copyRaw") },
	{ MODKEY,                       XK_w,	spawn,       SHCMD("$HOME/.local/bin/paste") },
	//{ 0,                       XK_F1,	spawn,       SHCMD("xdotool click 1") },
	//{ 0,                       XK_F2,	spawn,       SHCMD("xdotool click 2") },
	//{ 0,                       XK_F3,	spawn,       SHCMD("xdotool click 3") },
	//{ 0,                       XK_F4,	spawn,       SHCMD("xdotool mousedown 1") },
	//{ 0,                       XK_F5,	spawn,       SHCMD("xdotool mouseup 1") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

